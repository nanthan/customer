import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from  '@angular/forms';
import { Router } from '@angular/router';
import { CustomerserviceService } from '../services/customerservice.service';
import { first } from 'rxjs/operators';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private router:Router,private customerservice:CustomerserviceService ) { }

  loginForm: FormGroup;
  isSubmitted  =  false;
  size:string;

  ngOnInit() {
    this.loginForm  =  this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
  });
  }

  get f() { return this.loginForm.controls; }

  // login(){
  //   console.log(this.loginForm.value);
  //   this.isSubmitted = true;
  //   if(this.loginForm.invalid){
  //     return;
  //   }
  //   this.router.navigateByUrl('/home');
  // }

  login()
  {
    this.isSubmitted = true;

    if (this.loginForm.invalid) {
      return;
  }

    this.customerservice.userlogin(this.f.username.value,this.f.password.value)
      .pipe(first())
      .subscribe(
        (res:any) => {
          
          if(res.length==0){
            this.size="Username or Password incorrect";
            // alert('Email or Password incorrect');
          }else{
            this.router.navigateByUrl('/home');
          }
          // console.log(res.json());
        });
  }

}
