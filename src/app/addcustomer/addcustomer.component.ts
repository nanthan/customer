import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { FormGroup,FormBuilder, Validators} from '@angular/forms';
import { CustomerserviceService } from '../services/customerservice.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-addcustomer',
  templateUrl: './addcustomer.component.html',
  styleUrls: ['./addcustomer.component.css']
})
export class AddcustomerComponent implements OnInit {

  addform:FormGroup;
  addsubmit=false;
  constructor(private formBuilder: FormBuilder,private customerservise:CustomerserviceService,private route:Router) { }

  ngOnInit() {
    this.addform = this.formBuilder.group({
      names: ['',Validators.required],
      shopname:['',Validators.required],
      address:['',Validators.required]
    })
  }

  get f(){
    return this.addform.controls;
  }

  addsubmits(){
    this.addsubmit = true;

    if (this.addform.invalid) {
      return;
  }
    console.log(this.addform.value);
    this.customerservise.customerdata(this.f.names.value,this.f.shopname.value,this.f.address.value)
    .pipe(first())
    .subscribe(
      (res:any) => {
         
      });
    }

}
