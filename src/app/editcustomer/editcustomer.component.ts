import { Component, OnInit } from '@angular/core';

import { Router,ActivatedRoute } from '@angular/router';
import { FormGroup,FormBuilder, Validators} from '@angular/forms';
import { CustomerserviceService } from '../services/customerservice.service';
import { first } from 'rxjs/operators';
import { Customer } from '../customer';

@Component({
  selector: 'app-editcustomer',
  templateUrl: './editcustomer.component.html',
  styleUrls: ['./editcustomer.component.css']
})
export class EditcustomerComponent implements OnInit {

  editform:FormGroup;
  editsubmits=false;

  list:Customer;
  c_id:any;
  c_data:any;

  customerData: any = {};
  
  
  constructor(private formBuilder: FormBuilder,private customerservise:CustomerserviceService,private route:Router,
    private router: ActivatedRoute) { }

  ngOnInit() {
    this.c_id = this.router.snapshot.params['postId'];
    this.customerservise.find(this.c_id).subscribe((data: {})=>{
      this.customerData = data;
      console.log(this.customerData);
    })
    this.editform = this.formBuilder.group({
      c_name: ['',Validators.required],
      c_shopname:['',Validators.required],
      c_address:['',Validators.required]
    })
  }  

  get f(){
    return this.editform.controls;
  }

  editsubmit(){
    this.editsubmits = true;

    if (this.editform.invalid) {
      return;
  }
    console.log(this.editform.value);
    this.customerservise.editcustomer(this.c_id,this.f.c_name.value,this.f.c_shopname.value,this.f.c_address.value)
    .pipe(first())
    .subscribe(
      (res:any) => {
         
      });
    }

}
