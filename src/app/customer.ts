export interface Customer {

    id:number;
    name:string;
    shopname:string;
    address:string;
}
