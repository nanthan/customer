import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder,Validators } from '@angular/forms';

import { Router } from '@angular/router';
import { CustomerserviceService } from '../services/customerservice.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {


  regForm:FormGroup;
  submits = false;

  constructor(private formBuilder: FormBuilder,private router:Router,private customerservice:CustomerserviceService) { }

  ngOnInit() {
    this.regForm  =  this.formBuilder.group({
      username: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required]
  });
  }

  get f() { return this.regForm.controls; }


  register()
  {
    this.submits = true;

    if (this.regForm.invalid) {
      return;
  }

    this.customerservice.userregistration(this.f.username.value,this.f.email.value,this.f.password.value)
      .pipe(first())
      .subscribe(
        (res:any) => {
         
        });
  }

}
