import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { RouterModule } from '@angular/router';
// import { MaterialModule } from './material';
import { EditcustomerComponent } from './editcustomer/editcustomer.component';
import { HomeComponent } from './home/home.component';
import { AddcustomerComponent } from './addcustomer/addcustomer.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    EditcustomerComponent,
    HomeComponent,
    AddcustomerComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      {
      path:"",
      redirectTo:'login',
      pathMatch:'full'
    },
    {
      path:'login',
      component:LoginComponent
    },
    {
      path:'register',
      component:RegisterComponent
    },
    {
      path:'add',
      component:AddcustomerComponent
    },
    {
      path:'edit/:postId',
      component:EditcustomerComponent
    },
    {
      path:'home',
      component:HomeComponent
    }
  ]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
