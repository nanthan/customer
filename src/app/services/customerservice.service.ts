import { Injectable } from '@angular/core';

import { HttpClient,HttpHeaders } from '@angular/common/http';
import { GetApiurl } from '../services/parameters';
import { Customer} from '../customer';
import { login } from '../model/login'; 
import { map, catchError } from 'rxjs/operators';

import {  Observable,throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomerserviceService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  }

  constructor(private httpClient: HttpClient) { }


  getAll(): Observable<Customer[]> {
    return this.httpClient.get<Customer[]>(GetApiurl('viewdata.php'))
    .pipe(
      
    )
  }

  readPolicies(): Observable<Customer[]>{
    return this.httpClient.get<Customer[]>(GetApiurl('viewdata.php'));
  }


  public userlogin(username, password) {
    let url = GetApiurl('login.php');
    url = url + `?username=${username}&password=${password}`
    return this.httpClient.get<any>(url)
        .pipe(map(login => {
          console.log(login);
            return login;
        }));
}

public userregistration(username,email,passwords) {
  let regUrl = GetApiurl('register.php');
  regUrl = regUrl + `?username=${username}&email=${email}&password=${passwords}`
  return this.httpClient.get<any>(regUrl)
      .pipe(map(Register => {
          return Register;
      }));
}

public customerdata(name,shopname,address) {
  let regUrl = GetApiurl('addcustomer.php');
  regUrl = regUrl + `?name=${name}&shopname=${shopname}&address=${address}`
  return this.httpClient.get<any>(regUrl)
      .pipe(map(adddata => {
          return adddata;
      }));
}

public editcustomer(id,name,shopname,address) {
  let regUrl = GetApiurl('editcustomer.php');
  regUrl = regUrl + `?id=${id}&name=${name}&shopname=${shopname}&address=${address}`
  return this.httpClient.get<any>(regUrl)
      .pipe(map(editdata => {
          return editdata;
      }));
}

// getCustomerDetails(id){
//   return this.httpClient.get(GetApiurl('findedit.php')+ `?id=${id}`);
// }


public delete(id) {
  let regUrl = GetApiurl('delete.php');
  regUrl = regUrl + `?id=${id}`
  return this.httpClient.get<any>(regUrl)
      .pipe(map(del => {
          return del;
      }));
}

public find(id){
  let regUrl = GetApiurl('findedit.php');
  regUrl = regUrl + `?id=${id}`
  return this.httpClient.get<any>(regUrl)
        .pipe(map(srech =>{
          return srech;
        }));
}




}
