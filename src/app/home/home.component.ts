import { Component, OnInit } from '@angular/core';

import { CustomerserviceService } from '../services/customerservice.service';
import { Customer } from '../customer';
import { first } from 'rxjs/operators';
import { Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  posts: Customer[] = [];

  constructor( private customerservice:CustomerserviceService, private route:Router) { }

  ngOnInit(): void {
    this.customerservice.readPolicies().subscribe((data: Customer[])=>{
      this.posts = data;
      console.log(this.posts);
    })  
  }

  getNavigation(link, id){
    if(id === ''){
        this.route.navigate([link]);
    } else {
        this.route.navigate([link + '/' + id]);
    }
}

  delete(id){
    this.customerservice.delete(id).pipe(first()).subscribe(
      (res:any) =>{
          window.location.reload();
      });

    // this.customerservice.delete(id).subscribe((policy: cdata)=>{
    //   console.log("Customer deleted, ", policy);
    // });
  }

}
